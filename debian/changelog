php-invoker (6.0.0-1) experimental; urgency=medium

  * Upload new major to experimental

  [ Sebastian Bergmann ]
  * Bump copyright year
  * Prepare release

  [ David Prévot ]
  * Simplify build
  * Update copyright (years)
  * Update clean

 -- David Prévot <taffit@debian.org>  Sat, 08 Feb 2025 12:33:21 +0100

php-invoker (5.0.1-2) unstable; urgency=medium

  * Upload to unstable in sync with PHPUnit 11

 -- David Prévot <taffit@debian.org>  Fri, 10 Jan 2025 09:37:47 +0100

php-invoker (5.0.1-1) experimental; urgency=medium

  [ Sebastian Bergmann ]
  * Use PHPStan instead of Psalm
  * Prepare release

  [ David Prévot ]
  * Update Standards-Version to 4.7.0

 -- David Prévot <taffit@debian.org>  Sun, 14 Jul 2024 10:00:48 +0200

php-invoker (5.0.0-1) experimental; urgency=medium

  [ Sebastian Bergmann ]
  * Start development of next major version
  * Bump copyright year
  * Prepare release

  [ David Prévot ]
  * Update copyright (years)

 -- David Prévot <taffit@debian.org>  Sun, 04 Feb 2024 13:36:39 +0100

php-invoker (4.0.0-1) experimental; urgency=medium

  * Upload new major version to experimental

  [ Sebastian Bergmann ]
  * Drop support for PHP 7.3 and PHP 7.4
  * Drop support for PHP 8.0
  * Bump copyright year
  * Prepare release

  [ David Prévot ]
  * Set upstream metadata fields: Security-Contact.
  * Update standards version to 4.6.2, no changes needed.
  * Update copyright (years and license)
  * Ship upstream security notice

 -- David Prévot <taffit@debian.org>  Sat, 04 Feb 2023 00:40:28 +0100

php-invoker (3.1.1-3) unstable; urgency=medium

  * Simplify gbp import-orig (and check signature)
  * Use dh-sequence-phpcomposer instead of --with phpcomposer
  * Mark package as Multi-Arch: foreign
  * Install and use phpabtpl(1) autoloaders
  * Update Standards-Version to 4.6.1

 -- David Prévot <taffit@debian.org>  Sat, 09 Jul 2022 17:58:33 +0200

php-invoker (3.1.1-2) unstable; urgency=medium

  * Upload to unstable in sync with PHPUnit 9
  * Update watch file format version to 4.
  * Update Standards-Version to 4.5.1

 -- David Prévot <taffit@debian.org>  Sun, 20 Dec 2020 11:12:36 -0400

php-invoker (3.1.1-1) experimental; urgency=medium

  [ Sebastian Bergmann ]
  * Prepare release

  [ David Prévot ]
  * Rename main branch to debian/latest (DEP-14)

 -- David Prévot <taffit@debian.org>  Wed, 30 Sep 2020 14:46:27 -0400

php-invoker (3.1.0-1) experimental; urgency=medium

  [ Sebastian Bergmann ]
  * Prepare release

  [ Benjamin Cremer ]
  * Clear timer alarm in finally block

  [ David Prévot ]
  * Set Rules-Requires-Root: no.

 -- David Prévot <taffit@debian.org>  Fri, 07 Aug 2020 09:06:44 +0200

php-invoker (3.0.2-1) experimental; urgency=medium

  [ Sebastian Bergmann ]
  * Ignore tests etc. from archive exports
  * Support PHP 8 for https://github.com/sebastianbergmann/phpunit/issues/4325
  * Prepare release

  [ David Prévot ]
  * Document gbp import-ref usage
  * Use debhelper-compat 13
  * Simplify override_dh_auto_test

 -- David Prévot <taffit@debian.org>  Sun, 28 Jun 2020 11:02:44 -1000

php-invoker (3.0.0-1) experimental; urgency=medium

  * Upload version compatible with PHPUnit 9 to experimental

  [ Sebastian Bergmann ]
  * Bump copyright year
  * Prepare release

  [ David Prévot ]
  * debian/copyright: Update copyright (years)
  * debian/upstream/metadata:
    set fields Bug-Database, Bug-Submit, Repository, Repository-Browse
  * debian/control:
    + Drop versioned dependency satisfied in (old)stable
    + Update standards version to 4.5.0

 -- David Prévot <taffit@debian.org>  Fri, 07 Feb 2020 21:59:54 -1000

php-invoker (2.0.0-2) unstable; urgency=medium

  * Install in path compatible with namespace
  * Simplify clean target
  * Drop get-orig-source target
  * Use debhelper-compat 12
  * Drop now useless overrides
  * Extend d/clean for PHPUnit 8
  * Update Standards-Version to 4.4.0

 -- David Prévot <taffit@debian.org>  Sat, 24 Aug 2019 08:54:19 -1000

php-invoker (2.0.0-1) unstable; urgency=medium

  [ Sebastian Bergmann ]
  * Cleanup (Closes: #882906)

  [ David Prévot ]
  * Update copyright (years)
  * Drop now useless Autoload.php template
  * Replace Luis Uribe and Thomas Goirand from Uploaders (Closes: #894009)
  * Move project repository to salsa.d.o
  * Update Standards-Version to 4.1.3

 -- David Prévot <taffit@debian.org>  Thu, 29 Mar 2018 10:33:58 -1000

php-invoker (1.1.4-3) unstable; urgency=medium

  * Team upload
  * Update Standards-Version to 3.9.7
  * Improve homemade Autoload.php
  * Rebuild with recent pkg-php-tools for the PHP 7.0 transition

 -- David Prévot <taffit@debian.org>  Sat, 19 Mar 2016 16:49:09 -0400

php-invoker (1.1.4-2) unstable; urgency=medium

  * Team upload
  * Add override for php-timer

 -- David Prévot <taffit@debian.org>  Tue, 30 Jun 2015 08:49:34 -0400

php-invoker (1.1.4-1) unstable; urgency=medium

  * Team upload

  [ Sebastian Bergmann ]
  * Cleanup

  [ Henrique Moody ]
  * Update license and copyright in all files

  [ David Prévot ]
  * Update copyright (year)
  * Adapt package to upstream clean up

 -- David Prévot <taffit@debian.org>  Mon, 29 Jun 2015 18:52:46 -0400

php-invoker (1.1.3-1) unstable; urgency=medium

  * Team upload, to experimental to respect the freeze
  * Update copyright
  * Adapt packaging to composer source
  * Use canonical Vcs-* URLs
  * Bump standards version to 3.9.6
  * Add DEP-8 compliant tests

 -- David Prévot <taffit@debian.org>  Sun, 15 Mar 2015 16:27:32 -0400

php-invoker (1.1.0-1) unstable; urgency=low

    * Initial release (Closes: #669914)

 -- Thomas Goirand <zigo@debian.org>  Thu, 10 May 2012 12:52:33 +0000
